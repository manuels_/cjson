# CJSON

A JSON library written in C.

# Summary

* [Documentations](#documentations)
  * [Types](#types)
    * [JSONError](#jsonerror)
    * [JSONData](#jsondata)
    * [JSONObject](#jsonobject)
    * [JSONArray](#jsonarray)
  * [Parser](#parser)

# Documentations

* [Types](#types)
  * [JSONError](#jsonerror)
    * [JSONErrorToString](#jsonerrortostring)
  * [JSONData](#jsondata)
    * [JSONDataClean](#jsondataclean)
    * [JSONType](#jsontype)
    * [JSONTypeToString](#jsontypetostring)
  * [JSONObject](#jsonobject)
    * [JSONObjectType](#jsonobjecttype)
    * [JSONObjectInit](#jsonobjectinit)
    * [JSONObjectInitHash](#jsonobjectinithash)
    * [JSONObjectInitLinked](#jsonobjectinitlinked)
    * [JSONObjectClean](#jsonobjectclean)
    * [JSONObjectFree](#jsonobjectfree)
    * [JSONObjectGet](#jsonobjectget)
    * [JSONObjectGetP](#jsonobjectgetp)
    * [JSONObjectSet](#jsonobjectset)
    * [JSONObjectRemove](#jsonobjectremove)
    * [JSONObjectForEach](#jsonobjectforeach)
    * [JSONObjectMap](#jsonobjectmap)
  * [JSONArray](#jsonarray)
    * [JSONArrayType](#jsonarraytype)
    * [JSONArrayInit](#jsonarrayinit)
    * [JSONArrayInitTable](#jsonarrayinittable)
    * [JSONArrayInitLinked](#jsonarrayinitlinked)
    * [JSONArrayClean](#jsonarrayclean)
    * [JSONArrayFree](#jsonarrayfree)
    * [JSONArrayLength](#jsonarraylength)
    * [JSONArrayGet](#jsonarrayget)
    * [JSONArrayGetP](#jsonarraygetp)
    * [JSONArraySet](#jsonarrayset)
    * [JSONArrayRemove](#jsonarrayremove)
    * [JSONArrayPush](#jsonarraypush)
    * [JSONArrayPop](#jsonarraypop)
    * [JSONArrayForEach](#jsonarrayforeach)
    * [JSONArrayMap](#jsonarraymap)
* [Parser](#parser)
    * [JSONParse](#jsonparse)
  * [Stream Parser](#stream-parser)
    * [JSONParserState](#jsonparserstate)
    * [JSONParse](#jsonparsestream)
    * [JSONParserGetData](#jsonparsergetdata)
    * [JSONParserClean](#jsonparserclean)

## Types

### JSONError

Defined in `types.h` as :
```c
typedef enum JSONError {
	JE_NOERR,
	JE_ALLOC,
	JE_NULLPARAM,
	JE_OUTOFRANGE,
	JE_BADPARAM,
	JE_BADCHAR,
	JE_BADSTRING,
	JE_BADNUMBER,
	JE_BADOBJECT,
	JE_BADARRAY
} JSONError;
```

Type retuned by almost every function to indicate whether an error occured during the execution of the function.  
You can use [JSONErrorToString](#jsonerrortostring) to convert them to a human readable error.

#### JSONErrorToString

Declared in `types.h` as :
```c
const char *JSONErrorToString(JSONError error);
```

Converts an error code into a human readable error.

##### Parameters :
* [`JSONError`](#jsonerror) : The error to convert.

##### Returns :
Type : `const char *`
Human readable error.

---

### JSONData

Defined in `types.h` as :
```c
typedef struct JSONData {
	JSONType type;
	union {
		char *string;
		double number;
		unsigned char bool;
		struct __JSONObject *object;
		struct __JSONArray *array;
	};
} JSONData;
```

Represents a JSON data.  
It can be one of the [`JSONType`](#jsontype) types and can be accessed with its type's property :
| Type      |JSONData Property | Property type   |
|-----------|------------------|-----------------|
| String    |`number`          | `double`        |
| Number    |`bool`            | `unsigned char` |
| Boolean   |`string`          | `char *`        |
| Object    |`object`          | `JSONObject`    |
| Array     |`array`           | `JSONArray`     |
| null      |**-**             | **-**           |
| undefined |**-**             | **-**           |

##### Properties :
* [`JSONType`](#jsontype) `type` : Type of the data.
* `char *``string` : String type property.
* `double` `number` : Number type property
* `unsigned char` `bool` : Boolean type property.
* [`struct __JSONObject *`](#jsonobject)`object` : Same as [`JSONObject`](#jsonobject). Object type property.
* [`struct __JSONArray *`](#jsonarray)`array` : Same as [`JSONArray`](#jsonarray). Array type property.

#### JSONDataClean

Defined in `types.h` as :
```c
JSONError JSONDataClean(JSONData data);
```

Call the appropriate cleaning function :
| Type      | Cleaning function                     |
|-----------|---------------------------------------|
| String    | `free`(`stdlib.h`)                    |
| Object    | [`JSONObjectClean`](#jsonobjectclean) |
| Array     | [`JSONArrayClean`](#jsonarrayclean)   |
| Number    | **-**                                 |
| Boolean   | **-**                                 |
| null      | **-**                                 |
| undefined | **-**                                 |
If the type is String, make sure it has been allocated.

##### Parameters :
* [`JSONData`](#jsondata) `data` : Data to clean.

##### Returns :
Type : `JSONError`
`JE_NOERR` on success else the error value.

#### JSONType

Defined in `types.h` as :
```c
typedef enum {
	JT_NUMBER,
	JT_BOOL,
	JT_STRING,
	JT_OBJECT,
	JT_ARRAY,
	JT_NULL,
	JT_UNDEFINED
} JSONType;
```

They each represent the following types :
| Type      | Constant name  |
|-----------|----------------|
| String    | `JT_NUMBER`    |
| Number    | `JT_BOOL`      |
| Boolean   | `JT_STRING`    |
| Object    | `JT_OBJECT`    |
| Array     | `JT_ARRAY`     |
| null      | `JT_NULL`      |
| undefined | `JT_UNDEFINED` |

#### JSONTypeToString

Declared in `types.h` as :
```c
const char *JSONTypeToString(JSONType type);
```

Converts a [`JSONType`](#jsontype) into a human readable type string.

##### Parameters :
* [`JSONType`](#jsontype) `type` : Type to convert

##### Returns :
Type : `const char *`
A human readable type string.

---

### JSONObject

Declared in `object.h`

Represents a JSON Object.  
Internally it can be one of two types : a hash table (`JOT_HASH`) or a linked list (`JOT_LINKED`).  
It must be initialized with the function [`JSONObjectInit`](#jsonobjectinit) before any use and freed by the function [`JSONObjectClean`](#jsonobjectclean).

#### JSONObjectType

Defined in `object.h` as :
```c
typedef enum JSONObjectType {
	JOT_HASH,
	JOT_LINKED,
} JSONObjectType;
```

The [`JSONObject`](#jsonobject)'s internal types which you can choose from to get better performance depending on the use case.  
If you are lazy and don't want to think about it you can use `JOT_DEFAULT` which is a macro set to `JOT_HASH` by default.

#### JSONObjectInit

Declared in `object.h` as :
```c
JSONError JSONObjectInit(JSONObject *obj, JSONObjectType type);
```

Initializes the [`JSONObject`](#jsonobject) pointed by `obj`. The [`JSONObject`](#jsonobject) must later be passed to [`JSONObjectClean`](#jsonobjectclean).

You can alternatively use [`JSONObjectInitHash`](#jsonobjectinithash) or [`JSONObjectInitLinked`](#jsonobjectinitlinked) to have more control over the initialization.

##### Parameters :
* [`JSONObject *`](#jsonobject)`obj` : Points to the [`JSONObject`](#jsonobject) to initialize.
* [`JSONObjectType`](#jsonobjecttype) `type` : Internal type of the initialized [`JSONObject`](#jsonobject). If it is `JOT_HASH` it is initialize with a size of 32.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectInitHash

Declared in `object.h` as :
```c
JSONError JSONObjectInitHash(JSONObject *obj, unsigned long size);
```

Initializes the [`JSONObject`](#jsonobject) pointed by `obj` with the `JOT_HASH` internal type. The [`JSONObject`](#jsonobject) must later be passed to [`JSONObjectClean`](#jsonobjectclean).

See [`JSONObjectInit`](#jsonobjectinit) for more details.

##### Parameters :
* [`JSONObject *`](#jsonobject)`obj` : Points to the [`JSONObject`](#jsonobject) to initialize.
* `unsigned long` `size` : Size of the internal hash table used.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectInitLinked

Declared in `object.h` as :
```c
JSONError JSONObjectInitLinked(JSONObject *obj);
```

Initializes the [`JSONObject`](#jsonobject) pointed by `obj` with the `JOT_LINKED` internal type. The [`JSONObject`](#jsonobject) must later be passed to [`JSONObjectClean`](#jsonobjectclean).

See [`JSONObjectInit`](#jsonobjectinit) for more details.

##### Parameters :
* [`JSONObject *`](#jsonobject)`obj` : Points to the [`JSONObject`](#jsonobject) to initialize.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectClean

Declared in `object.h` as :
```c
JSONError JSONObjectClean(JSONObject obj);
```

Cleans `obj` and all of its items recursively using the function [`JSONDataClean`](#jsondataclean).
Make sure there are no item of type String has a string constant as value before using this function.

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) to free.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectFree

Declared in `object.h` as :
```c
JSONError JSONObjectFree(JSONObject obj);
```

Frees `obj`.
It only frees the internal memory allocated for `obj`. If you want to clean every item recursively, use [`JSONObjectClean`](#jsonobjectclean).

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) to free.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectGet

Declared in `object.h` as :
```c
JSONError JSONObjectGet(JSONObject obj, const char *key, JSONData *value);
```

Gets the value of the element that corresponds to `key` in `obj`.

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) to get the value from.
* `const char *``key` : Key of the element.
* [`JSONData *`](#jsondata)`value` : Points to a [`JSONData`](#jsondata) to which the value will be returned.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectGetP

Declared in `object.h` as :
```c
JSONError JSONObjectGetP(JSONObject obj, const char *key, JSONData **valuep);
```

Gets a pointer to the value of the element that corresponds to `key` in `obj`.

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) to get the value from.
* `const char *``key` : Key of the element.
* [`JSONData **`](#jsondata)`valuep` : Points to a [`JSONData *`](#jsondata) to which a pointer to the value will be returned.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectSet

Declared in `object.h` as :
```c
JSONError JSONObjectSet(JSONObject obj, const char *key, JSONData value);
```

Sets the value of the element that corresponds to `key` in `obj`.

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) whose element's value will be modified.
* `const char *``key` : Key of the element.
* [`JSONData`](#jsondata) `value` : New value of the element.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectRemove

Declared in `object.h` as :
```c
JSONError JSONObjectRemove(JSONObject obj, const char *key, JSONData *value);
```

Removes the element that corresponds to `key` in `obj`.


##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) whose element will be removed.
* `const char *``key` : Key of the element.
* [`JSONData *`](#jsondata)`value` : If it is `NULL`, nothing happens. Otherwise, the value of the removed element is returned through this parameter.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONObjectForEach

Declared in `object.h` as :
```c
JSONError JSONObjectForEach(JSONObject obj, int(*fn)(const char *, JSONData, JSONObject));
```

Executes `fn` on each element in `obj`.

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) to loop.
* `int(*``fn``)(``const char *``, `[`JSONData`](#jsondata)`, `[`JSONObject`](#jsonobject)`)` : Function executed on each element.
 * `const char *` : Key of the element. Do not change the string, it would cause undefined behaviour.
 * [`JSONData`](#jsondata) : Value of the element.
 * [`JSONObject`](#jsonobject) : `obj`.

#### JSONObjectMap

Declared in `object.h` as :
```c
JSONError JSONObjectMap(JSONObject obj, int(*fn)(const char *, JSONData *, JSONObject));
```

Executes `fn` on each element in `obj`. The elements of `obj` can be modified with this function because the first parameter of `fn` is a pointer to the element.

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) to loop.
* `int(*``fn``)(``const char *``, `[`JSONData`](#jsondata)`, `[`JSONObject`](#jsonobject)`)` : Function executed on each element.
 * `const char *` : Key of the element. Do not change the string, it would cause undefined behaviour.
 * [`JSONData *`](#jsondata) : Pointer to the value of the element, the value of the element can therefore be modified.
 * [`JSONObject`](#jsonobject) : `obj`.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

---

### JSONArray

Declared in `array.h`

Represents a JSON Array.
Internally it can be one of two types : a table of linked lists (`JAT_TABLE`) or a linked list (`JAT_LINKED`).  
It must be initialized with the function [`JSONArrayInit`](#jsonarrayinit) before any use and freed by the function [`JSONArrayClean`](#jsonarrayclean).

#### JSONArrayType

Declared in `array.h` as :
```c
typedef enum JSONArrayType {
	JAT_TABLE,
	JAT_LINKED,
} JSONArrayType;
```

The [`JSONArray`](#jsonarray)'s internal types which you can choose from to get better performance depending on the use case.  
If you are lazy and don't want to think about it you can use `JAT_DEFAULT` which is a macro set to `JAT_HASH` by default.

#### JSONArrayInit

Declared in `array.h` as :
```c
JSONError JSONArrayInit(JSONArray *arr, JSONArrayType type);
```

Initializes the [`JSONArray`](#jsonarray) pointed by `arr`. The [`JSONArray`](#jsonarray) must later be passed to [`JSONArrayClean`](#jsonarrayclean).

You can alternatively use [`JSONArrayInitTable`](#jsonarrayinithash) or [`JSONArrayInitLinked`](#jsonarrayinitlinked) to have more control over the initialization.

##### Parameters :
* [`JSONArray *`](#jsonarray)`arr` : Points to the [`JSONArray`](#jsonarray) to initialize.
* [`JSONArrayType`](#jsonarraytype) `type` : Internal type of the initialized [`JSONArray`](#jsonarray). If it is `JAT_LINKED`.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayInitTable

Declared in `array.h` as :
```c
JSONError JSONArrayInitTable(JSONArray *arr, unsigned long size);
```

Initializes the [`JSONArray`](#jsonarray) pointed by `arr` with the `JAT_TABLE` internal type. The [`JSONArray`](#jsonarray) must later be passed to [`JSONArrayClean`](#jsonarrayclean).

See [`JSONArrayInit`](#jsonarrayinit) for more details.

##### Parameters :
* [`JSONArray *`](#jsonarray)`arr` : Points to the [`JSONArray`](#jsonarray) to initialize.
* `unsigned long` `size` : Size of the internal table used.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayInitLinked

Declared in `array.h` as :
```c
JSONError JSONArrayInitLinked(JSONArray *arr);
```

Initializes the [`JSONArray`](#jsonarray) pointed by `arr` with the `JAT_LINKED` internal type. The [`JSONArray`](#jsonarray) must later be passed to [`JSONArrayClean`](#jsonarrayclean).

See [`JSONArrayInit`](#jsonarrayinit) for more details.

##### Parameters :
* [`JSONArray *`](#jsonarray)`arr` : Points to the [`JSONArray`](#jsonarray) to initialize.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayClean

Declared in `array.h` as :
```c
JSONError JSONArrayClean(JSONArray arr);
```

Cleans `arr` and all of its items recursively using the function [`JSONDataClean`](#jsondataclean).
Make sure there are no item of type String has a string constant as value before using this function.

##### Parameters :
* [`JSONArray`](#jsonarray) `arr` : [`JSONarray`](#jsonarray) to free.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayFree

Declared in `array.h` as :
```c
JSONError JSONArrayFree(JSONArray arr);
```

Frees `arr`.
It only frees the internal memory allocated for `arr`. If you want to clean every item recursively, use [`JSONArrayClean`](#jsonarrayclean).

##### Parameters :
* [`JSONArray`](#jsonarray) `arr` : [`JSONarray`](#jsonarray) to free.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayLength

Declared in `array.h` as :
```c
unsigned long JSONArrayLength(JSONArray arr);
```

Gets the length of `arr`.

##### Parameters :
* [`JSONarray`](#jsonarray) `arr` : [`JSONarray`](#jsonarray) whose length is returned. If `NULL`, `0` is returned.

##### Returns :
Type : `unsigned long`
`arr`'s length.

#### JSONArrayGet

Declared in `array.h` as :
```c
JSONError JSONArrayGet(JSONArray arr, unsigned long index, JSONData *value);
```

Gets the value of the element at `index` in `arr`.

##### Parameters :
* [`JSONarray`](#jsonarray) `arr` : [`JSONarray`](#jsonarray) from which the element is retrieved.
* `unsigned long` `index` : Index of the element.
* [`JSONData *`](#jsondata)`value` : Points to a [`JSONData`](#jsondata) to which the value of the element is returned.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayGetP

Declared in `array.h` as :
```c
JSONError JSONArrayGetP(JSONArray arr, unsigned long index, JSONData **valuep);
```

Gets a pointer to the value of the element at `index` in `arr`.

##### Parameters :
* [`JSONarray`](#jsonarray) `arr` : [`JSONarray`](#jsonarray) from which the element is retrieved.
* `unsigned long` `index` : Index of the element.
* [`JSONData **`](#jsondata)`valuep` : Points to a [`JSONData *`](#jsondata) to which a pointer to the value will be returned.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArraySet

Declared in `array.h` as :
```c
JSONError JSONArraySet(JSONArray arr, unsigned long index, JSONData value);
```

Sets the value if the element at `index` in `arr`.

##### Parameters :
* [`JSONArray`](#jsonarray) `arr` : [`JSONArray`](#jsonarray) whose element will be modified.
* `unsigned long` `index` : Index of the element.
* [`JSONData`] `value` : New value of the element.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayRemove

Declared in `array.h` as :
```c
JSONError JSONArrayRemove(JSONArray arr, unsigned long index, JSONData *value);
```

Removed the value of the element `index` in `arr`.

##### Parameters :
* [`JSONArray`](#jsonarray) `arr` : [`JSONArray`](#jsonarray) whose element will be removed.
* `unsigned long` `index` : Index of the element.
* [`JSONData *`](#jsondata)`value`: Points to a [`JSONData`](#jsondata) to which the value of the removed element will be returned. If it is `NULL`, it is not used.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayPush

Declared in `array.h` as :
```c
JSONError JSONArrayPush(JSONArray arr, JSONData value);
```

Adds a new element with value `value` at the end of `arr`.

##### Parameters :
* [`JSONArray`](#jsonarray) `arr` : [`JSONArray`](#jsonarray) To which the new element will be added.
* [`JSONData`](#jsondata) `value` : Value of the element that will be added.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayPop

Declared in `array.h` as :
```c
JSONError JSONArrayPop(JSONArray arr, JSONData *value);
```

Removes the last element of `arr`.

##### Parameters :
* [`JSONObject`](#jsonobject) `obj` : [`JSONObject`](#jsonobject) from which the last element will be removed.
* [`JSONData *`](#jsondata)`value` : Points to a [`JSONData`](#jsondata) to which the removed element is returned. If it is `NULL`, it is not used.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONArrayForEach

Declared in `array.h` as :
```c
JSONError JSONArrayForEach(JSONArray arr, int(*fn)(JSONData, unsigned long, JSONArray));
```

Executes `fn` on each element in `arr`.

##### Parameters :
* [`JSONArray`](#jsonarray) `arr` : [`JSONArray`](#jsonarray) to loop.
* `int(*``fn``)(`[`JSONData`](#jsondata)`, unsigned long, `[`JSONArray`](#jsonarray)`)` : Function executed on each element.
 * [`JSONData`](#jsondata) : Value of the element.
 * `unsigned long` : Index of the element.
 * [`JSONArray`](#jsonarray) : `arr`.

#### JSONArrayMap

Declared in `array.h` as :
```c
JSONError JSONArrayMap(JSONArray arr, int(*fn)(JSONData *, unsigned long, JSONArray));
```

Executes `fn` on each element in `arr`. The elements of `arr` can be modified with this function because the first parameter of `fn` is a pointer to the element.

##### Parameters :
* [`JSONArray`](#jsonarray) `arr` : [`JSONArray`](#jsonarray) to loop.
* `int(*``fn``)(`[`JSONData`](#jsondata)`, unsigned long, `[`JSONArray`](#jsonarray)`)` : Function executed on each element.
 * [`JSONData *`](#jsondata) : Pointer to the value of the element, the value of the element can therefore be modified.
 * `unsigned long` : Index of the element.
 * [`JSONArray`](#jsonarray) : `arr`.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

---


## Parser

#### JSONParse

Declared in `parser.h` as :
```c
JSONError JSONParse(const char *json, unsigned long length, JSONData *data);
```

Parse a JSON. And put the result in `data`.

##### Parameters :
* `const char *``json` : JSON buffer to be parsed.
* `unsigned long` `length` : Length of `json`.
* [`JSONData *`](#jsondata)`data` : Pointer to a [`JSONData`](#jsondata) where the result will be returned.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

### Stream Parser

Parse a JSON from a stream.

#### JSONParserState

Declared in `parser-stream.h`.

Used with the [`JSONParseStream`](#jsonparsestream) function to keep track of the parser state when parsing a stream.
It must be set to `JSON_PARSER_STATE_INIT` before any use.

#### JSONParseStream

Declared in `parser-stream.h` as :
```c
JSONError JSONParseStream(const char *json, unsigned long length, JSONParserState *parserState);
```

Parse a JSON stream. The JSON can be split into multiple function calls with the same `parserState` to keep track of the state of the parser.
`parserState` be cleaned with [`JSONParserGetData`](#jsonparsergetdata) or [`JSONParserClean`](#jsonparserclean) after being passed to this function to avoid memory leaks.

##### Parameters :
* `const char *``json` : JSON buffer to be parsed.
* `unsigned long` `length` : Length of `json`.
* [`JSONParserState *`](#jsonparserstate)`parserState` : Pointer to a [`JSONParserState`](#jsonparserstate) which indicates the current state of the parser.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONParserGetData

Declared in `parser-stream.h` as :
```c
JSONError JSONParserGetData(JSONParserState state, JSONData *data);
```

Retrieve the data of the parsed JSON that corresponds to `state` and cleans the `state`.
Because `state` is cleaned by the function it cannot be used later in any other function.

##### Parameters :
* [`JSONParserState`](#jsonparserstate) `state` : State corresponding to the JSON from which the data is to be retrieved.
* [`JSONData *`](#jsondata)`data` : Pointer to a [`JSONData`](#jsondata) where the data will be returned.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.

#### JSONParserClean

Declared in `parser-stream.h` as :
```c
JSONError JSONParserClean(JSONParserState state);
```

Cleans `state`.

##### Parameters :
* [`JSONParserState`](#jsonparserstate) `state` : State to be cleaned.

##### Returns :
Type : [`JSONError`](#jsonerror)
`JE_NOERR` on success else the error value.
