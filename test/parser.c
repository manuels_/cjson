#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"
#include "object.h"
#include "array.h"


int arrayPrint(JSONData data, unsigned long i, JSONArray obj);
int objectPrint(const char *key, JSONData data, JSONObject obj) {
	printf("%s : ", key);
	switch (data.type) {
		case JT_STRING:
			printf("string : \"%s\"\n", data.string);
			break;
		case JT_NUMBER:
			printf("number : %lf\n", data.number);
			break;
		case JT_BOOL:
			printf("bool : %s\n", data.bool ? "true" : "false");
			break;
		case JT_OBJECT:
			JSONObjectForEach(data.object, objectPrint);
			break;
		case JT_ARRAY:
			JSONArrayForEach(data.array, arrayPrint);
			break;
	}

	return 0;
}
int arrayPrint(JSONData data, unsigned long i, JSONArray obj) {
	printf("%ld : ", i);
	switch (data.type) {
		case JT_STRING:
			printf("string : \"%s\"\n", data.string);
			break;
		case JT_NUMBER:
			printf("number : %lf\n", data.number);
			break;
		case JT_BOOL:
			printf("bool : %s\n", data.bool ? "true" : "false");
			break;
		case JT_OBJECT:
			JSONObjectForEach(data.object, objectPrint);
			break;
		case JT_ARRAY:
			JSONArrayForEach(data.array, arrayPrint);
			break;
	}

	return 0;
}

int main(int argc, char **argv) {
	JSONError err = JE_NOERR;
	JSONData data;

	if (argc < 2) {
		fprintf(stderr, "Not enough parameters\n");
		return 1;
	}

	err = JSONParse(argv[1], strlen(argv[1]), &data);

	if (err != JE_NOERR) {
		printf("Error : %s\n", JSONErrorToString(err));
		return 1;
	}

	printf("%s\n", JSONTypeToString(data.type));
	switch (data.type) {
		case JT_STRING:
			printf("string : \"%s\"\n", data.string);
			break;
		case JT_NUMBER:
			printf("number : %lf\n", data.number);
			break;
		case JT_BOOL:
			printf("bool : %s\n", data.bool ? "true" : "false");
			break;
		case JT_OBJECT:
			JSONObjectForEach(data.object, objectPrint);
			break;
		case JT_ARRAY:
			JSONArrayForEach(data.array, arrayPrint);
			break;
	}

	JSONDataClean(data);

	return 0;
}
