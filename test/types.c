#include <stdio.h>

#include "array.h"
#include "object.h"


int fn(JSONData *data, unsigned long i, JSONArray arr) {
	printf("%d : value : %s : %lf\n", i, JSONTypeToString(data->type), data->number);
	return 0;
}
int fn1(const char *key, JSONData *data, JSONObject obj) {
	printf("%s : %s\n", key, data->string);
	return 0;
}

int main(int argc, char **argv) {
	JSONObject obj;
	JSONObjectInit(&obj, JOT_HASH);

	JSONData data;
	data.type = JT_STRING;
	data.string = "hello";
	JSONObjectSet(obj, "coucou", data);
	data.string = "lila";
	JSONObjectSet(obj, "coucou", data);

	JSONObjectGet(obj, "coucou", &data);
	printf("coucou : %s : %s\n", JSONTypeToString(data.type), data.string);
	JSONObjectRemove(obj, "coucou", NULL);
	JSONObjectGet(obj, "coucou", &data);
	printf("coucou : %s\n", JSONTypeToString(data.type));
	JSONObjectGet(obj, "test", &data);
	printf("test : %s\n", JSONTypeToString(data.type));

	JSONObjectSet(obj, "salut", (JSONData){.type=JT_STRING, .string="hello"});
	JSONObjectSet(obj, "coucou", (JSONData){.type=JT_STRING, .string="hi"});

	JSONObjectMap(obj, fn1);

	JSONObjectFree(obj);

	JSONArray arr;

	JSONArrayInit(&arr, JAT_TABLE);

	data.type = JT_NUMBER;
	data.number = 17;
	JSONArrayPush(arr, data);

	data.type = JT_NUMBER;
	data.number = 10000;
	JSONArrayPush(arr, data);

	JSONArrayPush(arr, (JSONData){ JT_NUMBER, .number = 100.0});
	JSONArrayPush(arr, (JSONData){ JT_NUMBER, .number = 200.0});
	JSONArrayPush(arr, (JSONData){ JT_NUMBER, .number = 300.0});
	JSONArrayPush(arr, (JSONData){ JT_NUMBER, .number = 400.0});
	JSONArrayPush(arr, (JSONData){ JT_NUMBER, .number = 500.0});
	JSONArrayPush(arr, (JSONData){ JT_NUMBER, .number = 600.0});
	JSONArrayPush(arr, (JSONData){ JT_NUMBER, .number = 700.0});

	data.type = JT_BOOL;
	data.bool = 1;
	JSONArraySet(arr, 0, data);

	JSONArrayPop(arr, &data);
	printf("Element : %s, %lf\n", JSONTypeToString(data.type), data.number);

	JSONArrayRemove(arr, 3, &data);
	printf("Element : %s, %lf\n", JSONTypeToString(data.type), data.number);

	JSONArrayRemove(arr, 3, &data);
	printf("Element : %s, %lf\n", JSONTypeToString(data.type), data.number);

	printf("Length : %d\n", JSONArrayLength(arr));

	JSONArrayMap(arr, fn);


	JSONArrayClean(arr);

	return 0;
}
