#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser-stream.h"
#include "object.h"
#include "array.h"


int arrayPrint(JSONData data, unsigned long i, JSONArray obj);
int objectPrint(const char *key, JSONData data, JSONObject obj) {
	printf("%s : ", key);
	switch (data.type) {
		case JT_STRING:
			printf("string : \"%s\"\n", data.string);
			break;
		case JT_NUMBER:
			printf("number : %lf\n", data.number);
			break;
		case JT_BOOL:
			printf("bool : %s\n", data.bool ? "true" : "false");
			break;
		case JT_OBJECT:
			JSONObjectForEach(data.object, objectPrint);
			break;
		case JT_ARRAY:
			JSONArrayForEach(data.array, arrayPrint);
			break;
	}

	return 0;
}
int arrayPrint(JSONData data, unsigned long i, JSONArray obj) {
	printf("%ld : ", i);
	switch (data.type) {
		case JT_STRING:
			printf("string : \"%s\"\n", data.string);
			break;
		case JT_NUMBER:
			printf("number : %lf\n", data.number);
			break;
		case JT_BOOL:
			printf("bool : %s\n", data.bool ? "true" : "false");
			break;
		case JT_OBJECT:
			JSONObjectForEach(data.object, objectPrint);
			break;
		case JT_ARRAY:
			JSONArrayForEach(data.array, arrayPrint);
			break;
	}

	return 0;
}

int main(int argc, char **argv) {
	JSONParserState state = JSON_PARSER_STATE_INIT;
	JSONError err = JE_NOERR;
	JSONData data;

	for (int i = 1; i < argc && err == JE_NOERR; i++) {
		err = JSONParseStream(argv[i], strlen(argv[i]), &state);
	}

	if (err != JE_NOERR) {
		printf("Error : %s\n", JSONErrorToString(err));
		return 1;
	}
	if ((err = JSONParserGetData(state, &data)) != JE_NOERR) {
		fprintf(stderr, "Failed to get the data!\n");
		return 2;
	}

	printf("%s\n", JSONTypeToString(data.type));
	switch (data.type) {
		case JT_STRING:
			printf("string : \"%s\"\n", data.string);
			break;
		case JT_NUMBER:
			printf("number : %lf\n", data.number);
			break;
		case JT_BOOL:
			printf("bool : %s\n", data.bool ? "true" : "false");
			break;
		case JT_OBJECT:
			JSONObjectForEach(data.object, objectPrint);
			break;
		case JT_ARRAY:
			JSONArrayForEach(data.array, arrayPrint);
			break;
	}

	JSONDataClean(data);

	return 0;
}
