CC = gcc

LIBS = -lm


FLAGS =


INCLUDE_DIR = include
INCLUDES = $(wildcard $(INCLUDE_DIR)/*.h)

SRC_DIR = src
SRCS = $(wildcard $(SRC_DIR)/*.c)

OBJ_DIR = obj
OBJS = $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(SRCS))

OUT_DIR = build
SHARED = $(OUT_DIR)/libjson.so
STATIC = $(OUT_DIR)/libjson.a

TEST_DIR = test
TESTS = $(wildcard $(TEST_DIR)/*.c)
TEST_OUTS = $(patsubst $(TEST_DIR)/%.c, $(TEST_DIR)/%, $(TESTS))


all: shared static

shared: $(SHARED)

static: $(STATIC)

tests: $(TEST_OUTS)


$(SHARED): $(OBJS) | $(OUT_DIR)
	$(CC) -shared $(OBJS) $(LIBS) -o $@ $(FLAGS)

$(STATIC): $(OBJS) | $(OUT_DIR)
	ar rcs $@ $(OBJS)


$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INCLUDE_DIR)/%.h | $(OBJ_DIR)
	$(CC) -c $< -o $@ -I$(INCLUDE_DIR) $(FLAGS)

$(TEST_DIR)/%: $(TEST_DIR)/%.c $(SHARED)
	$(CC) $< -o $@ -I$(INCLUDE_DIR) -L$(OUT_DIR) $(patsubst $(OUT_DIR)/lib%.so, -l%, $(SHARED)) -Wl,-rpath='$${ORIGIN}/../build/' $(FLAGS)


$(OUT_DIR):
	mkdir $@

$(OBJ_DIR):
	mkdir $@

$(INCLUDE_DIR)/%.h:
	@


.Phony: all, shared, static, clean, print_shared, print_static, print_includes, print_srcs, print_objs, print_out, print_tests

clean:
	$(RM) $(OBJS) $(SHARED) $(STATIC) $(TEST_OUTS)

print_shared:
	@echo $(SHARED)
print_static:
	@echo $(STATIC)
print_includes:
	@echo $(INCLUDES)
print_srcs:
	@echo $(SRCS)
print_objs:
	@echo $(OBJS)
print_out:
	@echo $(OUT)
print_tests:
	@echo $(TESTS)
