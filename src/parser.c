#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

#include "parser.h"
#include "object.h"
#include "array.h"


#define TRUE_STR "true"
#define TRUE_LEN strlen(TRUE_STR)

#define FALSE_STR "false"
#define FALSE_LEN strlen(FALSE_STR)

#define NULL_STR "null"
#define NULL_LEN strlen(NULL_STR)

#define UNDEFINED_STR "undefined"
#define UNDEFINED_LEN strlen(UNDEFINED_STR)

// String allocation rules
#define STRING_ALLOC_INIT 32
#define STRING_ALLOC_STEP(size) (size * 2)


static inline char hexToNum(const char c);


JSONError parseData(const char *json, unsigned long length, unsigned long *i, JSONData *data);
JSONError parseString(const char *json, unsigned long length, unsigned long *i, char **string);
JSONError parseTrue(const char *json, unsigned long length, unsigned long *i, unsigned char *bool);
JSONError parseFalse(const char *json, unsigned long length, unsigned long *i, unsigned char *bool);
JSONError parseObject(const char *json, unsigned long length, unsigned long *i, JSONObject *object);
JSONError parseArray(const char *json, unsigned long length, unsigned long *i, JSONArray *array);
JSONError parseNull(const char *json, unsigned long length, unsigned long *i);
JSONError parseUndefined(const char *json, unsigned long length, unsigned long *i);
JSONError parseNumber(const char *json, unsigned long length, unsigned long *i, double *number);


JSONError JSONParse(const char *json, unsigned long length, JSONData *data) {
	if (json == NULL || data == NULL) return JE_NULLPARAM;

	if (length == 0) return JE_BADPARAM;

	JSONError err = JE_NOERR;

	unsigned long i = 0;

	// Ignores all white space character at the start of the json
	while (isspace(json[i])) {
		i++;
		if (i >= length) return JE_BADPARAM;
	}

	if ((err = parseData(json, length, &i, data)) != JE_NOERR) return err;

	// Ignores all white space character at the start of the json
	i--;
	do {
		i++;
		if (i >= length) return JE_NOERR;
	} while (isspace(json[i]));

	return JE_BADCHAR;
}


JSONError parseData(const char *json, unsigned long length, unsigned long *i, JSONData *data) {
	if (json == NULL || i == NULL || data == NULL) return JE_NULLPARAM;

	// Get the type of the data and initializes the state
	switch (json[*i]) {
		// String
		case '"':
			data->type = JT_STRING;

			return parseString(json, length, i, &(data->string));
		// Bool
		// true
		case 't':
			data->type = JT_BOOL;

			return parseTrue(json, length, i, &(data->bool));
		// false
		case 'f':
			data->type = JT_BOOL;

			return parseFalse(json, length, i, &(data->bool));
		// Object
		case '{':
			data->type = JT_OBJECT;

			return parseObject(json, length, i, &(data->object));
		// Array
		case '[':
			data->type = JT_ARRAY;

			return parseArray(json, length, i, &(data->array));
		// null
		case 'n':
			data->type = JT_NULL;
			
			return parseNull(json, length, i);
		// undefined
		case 'u':
			data->type = JT_UNDEFINED;

			return parseUndefined(json, length, i);
		// Other
		default:
			// Number
			if (('0' <= json[*i] && json[*i] <= '9') || json[*i] == '-') {
				data->type = JT_NUMBER;

				return parseNumber(json, length, i, &(data->number));
			}

			return JE_BADCHAR;
	}
}
JSONError parseString(const char *json, unsigned long length, unsigned long *i, char **string) {
	if (json == NULL || i == NULL || string == NULL) return JE_NULLPARAM;

	if (json[(*i)++] != '"') return JE_BADPARAM;

	unsigned long len = 0;
	unsigned long alloc = STRING_ALLOC_INIT;

	*string = (char *)malloc(STRING_ALLOC_INIT);

#define appendChar(c) \
	if (len >= alloc) {\
		alloc = STRING_ALLOC_STEP(alloc);\
		*string = (char *)realloc(*string, alloc);\
	}\
	(*string)[len++] = c;

	for (; *i < length; (*i)++) {
		if (iscntrl(json[*i])) {
			free(*string);
			return JE_BADSTRING;
		}
		if (json[*i] == '\\') {
#define caseEsc(esc, c) case esc: appendChar(c); break;
			switch (json[++*i]) {
				caseEsc('"', '"')
				caseEsc('\\', '\\')
				caseEsc('/', '/')
				caseEsc('b', '\b')
				caseEsc('f', '\f')
				caseEsc('n', '\n')
				caseEsc('r', '\r')
				caseEsc('t', '\t')
				// Unicode escape sequence
				case 'u': {
					unsigned short unicode = 0;

					char u;

					for (char j = 0; j < 4; j++) {
						u = hexToNum(json[++*i]);

						if (u == -1) {
							free(*string);
							return JE_BADSTRING;
						}

						unicode <<= 4;
						unicode |= u;
					}

					if (unicode < 0x80) { // One byte
						appendChar((char)unicode);
					} else if (unicode < 0x800) { // Two bytes
						appendChar(0b11000000 | ((unicode >> 6) & 0b11111));
						appendChar(0b10000000 | (unicode & 0b111111));
					} else { // Three bytes
						appendChar(0b11100000 | ((unicode >> 12) & 0b1111));
						appendChar(0b10000000 | ((unicode >> 6) & 0b111111));
						appendChar(0b10000000 | (unicode & 0b111111));
					}
					
					break;
				}
				default:
					return JE_BADSTRING;
			}
#undef caseEsc
			continue;
		}
		if (json[*i] == '"') {
			appendChar(0);
			(*i)++;
			return JE_NOERR;
		}
		appendChar(json[*i]);
	}
	free(*string);
	return JE_BADSTRING;
#undef appendChar
}
JSONError parseTrue(const char *json, unsigned long length, unsigned long *i, unsigned char *bool) {
	if (json == NULL || i == NULL || bool == NULL) return JE_NULLPARAM;

	for (char j = 0; j < TRUE_LEN; j++, (*i)++) {
		if (*i >= length || json[*i] != TRUE_STR[j]) return JE_BADCHAR;
	}

	*bool = 1;

	return JE_NOERR;
}
JSONError parseFalse(const char *json, unsigned long length, unsigned long *i, unsigned char *bool) {
	if (json == NULL || i == NULL || bool == NULL) return JE_NULLPARAM;

	for (char j = 0; j < FALSE_LEN; j++, (*i)++) {
		if (*i >= length || json[*i] != FALSE_STR[j]) return JE_BADCHAR;
	}

	*bool = 0;

	return JE_NOERR;
}
JSONError parseObject(const char *json, unsigned long length, unsigned long *i, JSONObject *object) {
	if (json == NULL || i == NULL || object == NULL) return JE_NULLPARAM;

	if (json[*i] != '{') return JE_BADPARAM;

	JSONError err;

	if ((err = JSONObjectInit(object, JOT_DEFAULT)) != JE_NOERR) return err;

	do {
		(*i)++;
		if (*i >= length) {
			JSONObjectClean(*object);
			return JE_BADOBJECT;
		}
	} while (isspace(json[*i]));
	if (json[*i] == '}') {
		(*i)++;
		return JE_NOERR;
	}

	char *key;
	JSONData child;

	while (1) {
		if (json[*i] != '"') {
			JSONObjectClean(*object);
			return JE_BADCHAR;
		}

		if ((err = parseString(json, length, i, &key)) != JE_NOERR) {
			JSONObjectClean(*object);
			return err;
		}

		(*i)--;
		do {
			(*i)++;
			if (*i >= length) {
				JSONObjectClean(*object);
				free(key);
				return JE_BADOBJECT;
			}
		} while (isspace(json[*i]));
		if (json[*i] != ':') {
			JSONObjectClean(*object);
			free(key);
			return JE_BADCHAR;
		}

		do {
			(*i)++;
			if (*i >= length) {
				JSONObjectClean(*object);
				free(key);
				return JE_BADOBJECT;
			}
		} while (isspace(json[*i]));


		if ((err = parseData(json, length, i, &child)) != JE_NOERR) {
			JSONObjectClean(*object);
			free(key);
			return err;
		}

		if ((err = JSONObjectSet(*object, key, child)) != JE_NOERR) {
			JSONObjectClean(*object);
			JSONDataClean(child);
			free(key);
			return err;
		}
		free(key);

		while (isspace(json[*i])) {
			(*i)++;
			if (*i >= length) {
				JSONObjectClean(*object);
				return JE_BADOBJECT;
			}
		}

		switch (json[*i]) {
			case ',':
				do {
					(*i)++;
					if (*i >= length) {
						JSONObjectClean(*object);
						return JE_BADOBJECT;
					}
				} while (isspace(json[*i]));
				continue;

			case '}':
				(*i)++;
				break;
			default:
				JSONObjectClean(*object);
				return JE_BADCHAR;
		}

		return JE_NOERR;
	}
}
JSONError parseArray(const char *json, unsigned long length, unsigned long *i, JSONArray *array) {
	if (json == NULL || i == NULL || array == NULL) return JE_NULLPARAM;

	if (json[*i] != '[') return JE_BADPARAM;

	JSONError err;

	if ((err = JSONArrayInit(array, JAT_DEFAULT)) != JE_NOERR) return err;

	do {
		(*i)++;
		if (*i >= length) {
			JSONArrayClean(*array);
			return JE_BADARRAY;
		}
	} while (isspace(json[*i]));
	if (json[*i] == ']') {
		(*i)++;
		return JE_NOERR;
	}

	JSONData child;

	while (1) {
		if ((err = parseData(json, length, i, &child)) != JE_NOERR) {
			JSONArrayClean(*array);
			return err;
		}

		if ((err = JSONArrayPush(*array, child)) != JE_NOERR) {
			JSONArrayClean(*array);
			JSONDataClean(child);
			return err;
		}

		(*i)--;
		do {
			(*i)++;
			if (*i >= length) {
				JSONArrayClean(*array);
				return JE_BADARRAY;
			}
		} while (isspace(json[*i]));

		switch (json[*i]) {
			case ',':
				do {
					(*i)++;
					if (*i >= length) {
						JSONArrayClean(*array);
						return JE_BADARRAY;
					}
				} while (isspace(json[*i]));
				continue;

			case ']':
				(*i)++;
				break;
			default:
				JSONArrayClean(*array);
				return JE_BADCHAR;
		}

		return JE_NOERR;
	}
}
JSONError parseNull(const char *json, unsigned long length, unsigned long *i) {
	if (json == NULL || i == NULL) return JE_NULLPARAM;

	for (char j = 0; j < NULL_LEN; j++, (*i)++) {
		if (*i >= length || json[*i] != NULL_STR[j]) return JE_BADCHAR;
	}

	return JE_NOERR;
}
JSONError parseUndefined(const char *json, unsigned long length, unsigned long *i) {
	if (json == NULL || i == NULL) return JE_NULLPARAM;

	for (char j = 0; j < UNDEFINED_LEN; j++, (*i)++) {
		if (*i >= length || json[*i] != UNDEFINED_STR[j]) return JE_BADCHAR;
	}

	return JE_NOERR;
}
JSONError parseNumber(const char *json, unsigned long length, unsigned long *i, double *number) {
	if (json == NULL || i == NULL || number == NULL) return JE_NULLPARAM;

	unsigned char neg = 0,
				  dot = 0,
				  exp = 0,
				  afterE = 0,
				  negE = 0,
				  ready = 0;
	unsigned long temp = 0,
				  e = 0,
				  nbdec = 0;

	if (json[*i] == '-') {
		neg = 1;
		(*i)++;
	}

	for (; *i < length; (*i)++) {
		if (afterE) {
			afterE = 0;
			switch (json[*i]) {
				case '-':
					negE = 1;
				case '+':
					continue;
			}
		}
		if ('0' <= json[*i] && json[*i] <= '9') {
			ready = 1;

			if (exp) {
				e *= 10;
				e += json[*i] - '0';
				continue;
			}

			temp *= 10;
			temp += json[*i] - '0';

			if (dot) nbdec += 1;

			continue;
		}
		if (!dot && !exp && json[*i] == '.') {
			dot = 1;
			ready = 0;
			continue;
		}
		if (!exp && json[*i] == 'e') {
			exp = 1;
			afterE = 1;
			ready = 0;
			continue;
		}
		break;
	}
	if (!ready) return JE_BADCHAR;

	*number = (double)(temp) * pow(10, (long)(e * (negE ? -1 : 1) - nbdec)) * (neg ? -1 : 1);

	return JE_NOERR;
}



static inline char hexToNum(const char c) {
	return '0' <= c && c <= '9' ? c - '0' :
		'A' <= c && c <= 'F' ? c - 'A' + 10 :
		'a' <= c && c <= 'f' ? c - 'a' + 10 :
		-1;
}
