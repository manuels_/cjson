#include <stdlib.h>
#include <string.h>

#include "array.h"


// Linked list item
typedef struct __ArrLinked {
	JSONData value;
	struct __ArrLinked *next;
} __ArrLinked;

#define JSON_ARRAY_TABLE_DEFAULT_SIZE 32

// Definition of struct __JSONArray which is used internally
struct __JSONArray {
	// JSONArray type used internally
	JSONArrayType type;
	// Length of the array
	unsigned long length;
	union {
		struct {
			// Allocated table size
			unsigned long tableSize;
			__ArrLinked **table;
		};
		__ArrLinked *linkedList;
	};
};


// Functions to free a linked list and a linked list item
JSONError __freeArrLinked(__ArrLinked *linked);


// Initialize the JSONArray with the given type
JSONError JSONArrayInit(JSONArray *arr, JSONArrayType type) {
	if (arr == NULL) return JE_NULLPARAM;
	*arr = NULL; // So it is NULL if the function fail to prevent it from being used afterwards

	switch (type) {
		// Table type
		case JAT_TABLE:
			return JSONArrayInitTable(arr, JSON_ARRAY_TABLE_DEFAULT_SIZE);
		// Linked array type
		case JAT_LINKED:
			return JSONArrayInitLinked(arr);
		default:
			return JE_BADPARAM;
	}
}
// Initialize the JSONArray with the table type
JSONError JSONArrayInitTable(JSONArray *arr, unsigned long size) {
	if (arr == NULL) return JE_NULLPARAM;
	*arr = NULL;

	*arr = (JSONArray)malloc(sizeof(struct __JSONArray));
	if (*arr == NULL) return JE_ALLOC;
	(*arr)->type = JAT_TABLE;
	(*arr)->length = 0;

	(*arr)->table = (__ArrLinked **)malloc(sizeof(__ArrLinked *) * size);
	if ((*arr)->table == NULL) {
		free(*arr);
		return JE_ALLOC;
	}
	(*arr)->tableSize = size;
	// Set all the linked lists to NULL
	memset((*arr)->table, 0, sizeof(__ArrLinked *) * size);

	return JE_NOERR;
}
// Initialize the JSONArray with the linked array type
JSONError JSONArrayInitLinked(JSONArray *arr) {
	if (arr == NULL) return JE_NULLPARAM;
	*arr = NULL;

	*arr = (JSONArray)malloc(sizeof(struct __JSONArray));
	if (*arr == NULL) return JE_ALLOC;
	(*arr)->type = JAT_LINKED;
	(*arr)->length = 0;

	(*arr)->linkedList = NULL;
	
	return JE_NOERR;
}
// Clean the JSONArray
JSONError JSONArrayClean(JSONArray arr) {
	if (arr == NULL) return JE_NULLPARAM;

	int freeData(JSONData data, unsigned long i, JSONArray arr) {
		JSONDataClean(data);
		return 0;
	}

	JSONArrayForEach(arr, freeData);
	return JSONArrayFree(arr);
}
// Free the JSONArray
JSONError JSONArrayFree(JSONArray arr) {
	if (arr == NULL) return JE_NULLPARAM;

	JSONError err = JE_NOERR;
	switch (arr->type) {
		case JAT_TABLE:
			for (unsigned long i = 0; i < arr->length; i++) 
				err |= __freeArrLinked(arr->table[i]);
			break;
		case JAT_LINKED:
			err = __freeArrLinked(arr->linkedList);
			break;
		default:
			return JE_BADPARAM;
	}

	free(arr);

	return err;
}
// Length of the array
unsigned long JSONArrayLength(JSONArray arr) {
	if (arr == NULL) return 0;

	return arr->length;
}
// Get a pointer to the item at the specified index in the JSONArray
JSONError JSONArrayGetP(JSONArray arr, unsigned long index, JSONData **valuep) {
	if (arr == NULL || valuep == NULL) return JE_NULLPARAM;

	__ArrLinked *item;

	switch (arr->type) {
		// Table type
		case JAT_TABLE:
			// Get the right linked list in the table
			item = arr->table[index % arr->tableSize];
			// Set the index to the index in the linked list
			index = index / arr->tableSize;
			break;
		// Linked list type
		case JAT_LINKED:
			item = arr->linkedList;
			break;
		// Invalid type
		default:
			return JE_BADPARAM;
	}

	// Loop until the index
	for (unsigned long i = 0; i < index && item != NULL; i++)
		item = item->next;

	if (item == NULL) return JE_OUTOFRANGE;

	*valuep = &(item->value);

	return JE_NOERR;
}
// Get the item at the specified index in the JSONArray
JSONError JSONArrayGet(JSONArray arr, unsigned long index, JSONData *value) {
	if (arr == NULL || value == NULL) return JE_NULLPARAM;

	JSONData *data;

	JSONError err = JSONArrayGetP(arr, index, &data);

	if (err != JE_NOERR) return err;

	*value = *data;

	return JE_NOERR;
}
// Set the item at the specific index in the JSONArray
JSONError JSONArraySet(JSONArray arr, unsigned long index, JSONData value) {
	if (arr == NULL) return JE_NULLPARAM;

	JSONData *data;

	JSONError err = JSONArrayGetP(arr, index, &data);

	if (err != JE_NOERR) return err;

	*data = value;

	return JE_NOERR;
}
// Remove the item at the specified index from the JSONArray
JSONError JSONArrayRemove(JSONArray arr, unsigned long index, JSONData *value) {
	if (arr == NULL) return JE_NULLPARAM;

	__ArrLinked **itemP;
	unsigned long i;

	switch (arr->type) {
		// Table type
		case JAT_TABLE:
			// Index of the linked list which corresponds to the index
			unsigned long linkedIndex = index % arr->tableSize;
			// Set the index to the index in the linked list
			index = index / arr->tableSize;

			__ArrLinked *afterEl, // The element ofter the element to remove
				**prevItem = &(arr->table[linkedIndex]); // The element that was previously moved (see bellow to understand)
			// Loop until the index of the element to remove
			for (i = 0; i < index && *prevItem != NULL; i++)
				prevItem = &((*prevItem)->next);
			// Out of range
			if (*prevItem == NULL) return JE_OUTOFRANGE;

			// We get the element after the element to remove
			afterEl = (*prevItem)->next;

			if (value != NULL) *value = (*prevItem)->value;
			// Free the element to remove
			free(*prevItem);

			arr->length -= 1;

			// Shift linked lists
			unsigned long j;
			// Loop through each linked list from the linked list after the one that contains the element to remove to the end
			for (i = linkedIndex + 1; i < arr->tableSize; i++) {
				// Current linked list
				itemP = &(arr->table[i]);

				for (j = 0; j < index && *itemP != NULL; i++)
					itemP = &((*itemP)->next);
				// Move the linked list to where the previously moved list was
				*prevItem = *itemP;
				// End of array
				if (*itemP == NULL) return JE_NOERR;

				prevItem = itemP;
			}

			index += 1;
			// Loop through each linked list from the start to the linked list before the one that contains the element to remove
			for (i = 0; i < linkedIndex; i++) {
				// Current linked list
				itemP = &(arr->table[i]);

				for (j = 0; j < index && *itemP != NULL; i++)
					itemP = &((*itemP)->next);
				// Move the linked list to where the previously moved list was
				*prevItem = *itemP;
				// End of array
				if (*itemP == NULL) return JE_NOERR;

				prevItem = itemP;
			}
			// Move the list after the removed element to where the previously moved list was
			*prevItem = afterEl;

			return JE_NOERR;
		// Linked list type
		case JAT_LINKED:
			itemP = &(arr->linkedList);

			// Loop until the index
			for (i = 0; i < index && *itemP != NULL; i++)
				itemP = &((*itemP)->next);

			if (*itemP == NULL) return JE_OUTOFRANGE;

			__ArrLinked *item = *itemP;

			*itemP = item->next;

			if (value != NULL) *value = item->value;

			free(item);

			arr->length -= 1;

			return JE_NOERR;
		// Invalid type
		default:
			return JE_BADPARAM;
	}
	return JE_NOERR;
}
// Push at the value at the end of the array
JSONError JSONArrayPush(JSONArray arr, JSONData value) {
	if (arr == NULL) return JE_NULLPARAM;

	__ArrLinked **itemP;

	switch (arr->type) {
		// Table type
		case JAT_TABLE:
			// Get the right linked list in the table
			itemP = &(arr->table[arr->length % arr->tableSize]);
			break;
		// Linked list type
		case JAT_LINKED:
			itemP = &(arr->linkedList);
			break;
		// Invalid type
		default:
			return JE_BADPARAM;
	}
	// Loop to the last item
	while (*itemP != NULL)
		itemP = &((*itemP)->next);


	*itemP = (__ArrLinked*)malloc(sizeof(__ArrLinked));

	(*itemP)->value = value;
	(*itemP)->next = NULL;

	arr->length += 1;

	return JE_NOERR;
}
// Pop the last element from the array and returns it if value is not NULL
JSONError JSONArrayPop(JSONArray arr, JSONData *value) {
	if (arr == NULL) return JE_NULLPARAM;

	if (arr->length == 0) return JE_OUTOFRANGE;

	__ArrLinked **itemP;

	switch (arr->type) {
		// Table type
		case JAT_TABLE:
			// Get the right linked list in the table
			itemP = &(arr->table[(arr->length - 1) % arr->tableSize]);
			break;
		// Linked list type
		case JAT_LINKED:
			itemP = &(arr->linkedList);
			break;
		// Invalid type
		default:
			return JE_BADPARAM;
	}

	if (*itemP == NULL)
		return JE_OUTOFRANGE;
	// Loop to the last item
	while ((*itemP)->next != NULL )
		itemP = &((*itemP)->next);

	if (value != NULL) *value = (*itemP)->value;

	free(*itemP);

	*itemP = NULL;

	arr->length -= 1;

	return JE_NOERR;
}
// Internal ForEach because JSONObjectForEach and JSONObjectMap are almost identical
JSONError __ArrForEach(JSONArray arr, int(*fn)(), int pointerArg) {
	if (arr == NULL || fn == NULL) return JE_NULLPARAM;

	__ArrLinked *item;

	unsigned long i;
	switch (arr->type) {
		// Table type
		case JAT_TABLE: {
			__ArrLinked **lists = (__ArrLinked **)malloc(sizeof(__ArrLinked *) * arr->tableSize);

			memcpy(lists, arr->table, sizeof(__ArrLinked *) * arr->tableSize);

			i = 0;
			while (1) {
				for (unsigned long j = 0; j < arr->tableSize; j++, i++) {
					item = lists[j];

					if (item == NULL) goto break_while;

					if (pointerArg) {
						if (fn(&(item->value), i, arr)) goto break_while;
					} else if (fn(item->value, i, arr)) goto break_while;

					lists[j] = item->next;
				}
			}
break_while:
			free(lists);

			break;
		}
		// Linked list type
		case JAT_LINKED:
			for (i = 0, item = arr->linkedList; item != NULL; i++, item = item->next) {
				if (pointerArg) {
					if (fn(&(item->value), i, arr)) break;
				} else if (fn(item->value, i, arr)) break;
			}

			break;
		default:
			return JE_BADPARAM;
	}

	return JE_NOERR;
}
// Calls the function for each element in the array
JSONError JSONArrayForEach(JSONArray arr, int(*fn)(JSONData, unsigned long, JSONArray)) {
	return __ArrForEach(arr, (int(*)())fn, 0);
}
// Calls the function for each element in the array passing a pointer to the JSONData so it can be modified
JSONError JSONArrayMap(JSONArray arr, int(*fn)(JSONData *, unsigned long, JSONArray)) {
	return __ArrForEach(arr, (int(*)())fn, 1);
}


JSONError __freeArrLinked(__ArrLinked *linked) {
	__ArrLinked *item;

	// Loop through all the items
	while (linked != NULL) {
		item = linked; // Save it in another variable because if I free the item before getting the next item it will cause indefined behaviour

		linked = linked->next;

		free(item);
	}

	return JE_NOERR;
}
