#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "object.h"
#include "array.h"


const char *JSONErrorToString(JSONError error) {
	switch (error) {
		case JE_NOERR:		return "No error";
		case JE_ALLOC:		return "Allocation error";
		case JE_NULLPARAM:	return "Unexpected a null pointer as parameter";
		case JE_OUTOFRANGE:	return "Index out of range";
		case JE_BADPARAM:	return "Invalid parameter";
		case JE_BADCHAR:	return "Invalid character";
		case JE_BADSTRING:	return "Invalid String";
		case JE_BADNUMBER:	return "Invalid Number";
		case JE_BADOBJECT:	return "Invalid Object";
		case JE_BADARRAY:	return "Invalid Array";
		default:		return "No description for this error";
	}
}


const char *JSONTypeToString(JSONType type) {
	switch (type) {
		case JT_NUMBER:		return "Number";
		case JT_BOOL:		return "Boolean";
		case JT_STRING:		return "String";
		case JT_OBJECT:		return "Object";
		case JT_ARRAY:		return "Array";
		case JT_NULL:		return "null";
		case JT_UNDEFINED:	return "undefined";
		default:		return "Invalid type";
	}
}


JSONError JSONDataClean(JSONData data) {
	switch (data.type) {
		// Nothing to clean
		case JT_NUMBER:
		case JT_BOOL:
		case JT_NULL:
		case JT_UNDEFINED:
			break;
		case JT_STRING:
			free(data.string);
			break;
		case JT_OBJECT:
			return JSONObjectClean(data.object);
		case JT_ARRAY:
			return JSONArrayClean(data.array);
		default:
			return JE_BADPARAM;
	}
}
