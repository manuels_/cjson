#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

#include "parser-stream.h"
#include "object.h"
#include "array.h"


// Shorter to write
#define STATE (*parserState)
#define DATA (STATE->data)
#define STEP (STATE->step)
#define STRING_STATE (STATE->string)
#define NUMBER_STATE (STATE->number)
#define BOOL_STATE (STATE->bool)
#define OBJECT_STATE (STATE->object)
#define ARRAY_STATE (STATE->array)
#define NULL_STATE (STATE->null)
#define UNDEFINED_STATE (STATE->undefined)

#define TRUE_STR "true"
#define FALSE_STR "false"
#define NULL_STR "null"
#define UNDEFINED_STR "undefined"

// String allocation rules
#define STRING_ALLOC_INIT 32
#define STRING_ALLOC_STEP(size) (size * 2)


// What the parser is currently doing
enum __Step {
	PS_START,	// Start of the json, waiting for the first non white space character to identify the type
	PS_STRING,	// Parsing a string
	PS_NUMBER,	// Parsing a number
	PS_BOOL,	// Parsing a bool
	PS_OBJECT,	// Parsing an object
	PS_ARRAY,	// Parsing an array
	PS_NULL,	// Parsing a null
	PS_UNDEFINED,	// Parsing an undefined
	PS_END,		// End of the json, checking that there is no non white space character
};
// Parsing a string
struct __StringState {
	unsigned long length;		// Length of the string
	unsigned long allocSize;	// Size of the allocated memory for the string
	unsigned short unicode;		// Value of the unicode character being parsed
	unsigned char esc : 1,		// Parsing an escape sequence
		      uni : 1,		// Parsing a unicode escape sequence
		      uniLen : 3;	// Length of the unicode escape sequence that is being parsed, should be 4 at the end
};
// Parsing a number
struct __NumberSate {
	unsigned long tmp;		// Number being parsed as big integer, including the decimal part
	unsigned long e;		// Power of ten of the number
	unsigned long decimalCount;	// Number of decimals
	unsigned char negative : 1,	// The number is negative
		      dot : 1,		// The number has a decimal part
		      exp : 1,		// The number has a power of ten
		      afterExp : 1,	// Previous character was a 'e' character, to detect if there is a negative exponent
		      negExp : 1,	// The exponent is negative
		      ready : 1;	// If the number is in a state where it could be retrieved by the function JSONParserGetData, this variable is necessary because we never know when a number really ends if it is inside multiple buffers at ounce
// examples : "535." "23" : the number is not ready at the end of the first buffer
//            "264" "235" : the number is ready at the end of the first buffer but, it may not be finished as we can see with the second buffer
};
// Parsing bool
struct __BoolState {
	unsigned long length;	// Length of bool identifier being parsed
};
// Parsing object
enum __ObjectStep {
	OS_START,	// Start of the object, waiting for a key or a '}'
	OS_COLON,	// After the key, waiting for a ':'
	OS_END,		// After the value, waiting for a ',' or a '}'
	OS_AFTER	// After a ',', waiting for a key
};
struct __ObjectState {
	char *key;		// Key of the item currently being parsed
	enum __ObjectStep step;	// Step in the parsing
};
// Parsing array
enum __ArrayStep {
	AS_START,	// Start of the array, waiting for a value or a ']'
	AS_END,		// After the value, waiting for a ',' or a ']'
};
struct __ArrayState {
	enum __ArrayStep step;	// Step in the parsing
};
// Parsing null
struct __nullState {
	unsigned long length;	// Length of the null identifier being parsed
};
// Parsing undefined
struct __undefinedState {
	unsigned long length;	// Length of the undefined identifer being parsed
};
// Parser state
struct __ParserState {
	enum __Step step;				// Step in the parsing
	JSONData data;					// Value of the parsed json
	union {
		struct __StringState string;		// String state
		struct __NumberSate number;		// Number state
		struct __BoolState bool;		// Bool state
		struct __ObjectState object;		// Object state
		struct __ArrayState array;		// Array state
		struct __nullState null;		// Null state
		struct __undefinedState undefined;	// Undefined state
	};
	struct __ParserState *parent;			// Parent of the parsed item, an object or an array
};


// Hexadecimal to number conversion
static inline char hexToNum(const char c);


// Free The state, including the data
JSONError __freeState(struct __ParserState *state);

// Parse different steps
static JSONError parseStartStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseStringStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseNumberStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseBoolStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseObjectStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseArrayStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseNullStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseUndefinedStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);
static JSONError parseEndStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState);

// The parser main function
JSONError JSONParseStream(const char *json, unsigned long length, struct __ParserState **parserState) {
	if (json == NULL || parserState == NULL) return JE_NULLPARAM;

	// Initializing state
	if (STATE == JSON_PARSER_STATE_INIT) {
		STATE = (struct __ParserState *)malloc(sizeof(struct __ParserState));
		STEP = PS_START;
		STATE->parent = NULL;
		DATA.type = JT_UNDEFINED;
	}

	unsigned long i = 0;
	JSONError err;

	for (; i < length; i++) {
		switch (STEP) {
			case PS_START:
				err = parseStartStream(json, length, &i, parserState);
				break;
			case PS_STRING:
				err = parseStringStream(json, length, &i, parserState);
				break;
			case PS_NUMBER:
				err = parseNumberStream(json, length, &i, parserState);
				break;
			case PS_BOOL:
				err = parseBoolStream(json, length, &i, parserState);
				break;
			case PS_OBJECT:
				err = parseObjectStream(json, length, &i, parserState);
				break;
			case PS_ARRAY:
				err = parseArrayStream(json, length, &i, parserState);
				break;
			case PS_NULL:
				err = parseNullStream(json, length, &i, parserState);
				break;
			case PS_UNDEFINED:
				err = parseUndefinedStream(json, length, &i, parserState);
				break;
			case PS_END:
				err = parseEndStream(json, length, &i, parserState);
				break;
			default:
				err = JE_BADCHAR;
		}

		if (err != JE_NOERR) {
			JSONParserClean(STATE);
			STATE = NULL;
			return err;
		}
	}

	return JE_NOERR;
}

// Clean a state
JSONError JSONParserClean(struct __ParserState *state) {
	if (state == NULL) return JE_NULLPARAM;

	struct __ParserState *tmp;

	while (state != NULL) {
		tmp = state->parent;
		__freeState(state);
		state = tmp;
	}

	return JE_NOERR;
}
// Retrieve the parsed data, cleans the state even if the data could not be retrieved
JSONError JSONParserGetData(struct __ParserState *state, JSONData *data) {
	if (state == NULL || data == NULL) return JE_NULLPARAM;

	// Check if the data is ready to be retrieved
	if (((state->step != PS_END && state->step != PS_NUMBER) || (state->step == PS_NUMBER && !state->number.ready)) || state->parent != NULL) {
		JSONParserClean(state);
		return JE_BADPARAM;
	}

	*data = state->data;

	free(state);

	return JE_NOERR;
}


JSONError parseStartStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	// Ignores all white space character at the start of the json
	while (*i < length && isspace(json[*i])) (*i)++;
	if (*i >= length) return JE_NOERR;

	// Get the type of the data and initializes the state
	switch (json[*i]) {
		// String
		case '"':
			STEP = PS_STRING;
			STRING_STATE.esc = 0;
			STRING_STATE.length = 0;
			STRING_STATE.allocSize = STRING_ALLOC_INIT;
			
			DATA.type = JT_STRING;
			DATA.string = (char *)malloc(STRING_ALLOC_INIT);

			break;
		// Bool
		// true
		case 't':
			STEP = PS_BOOL;
			BOOL_STATE.length = 1;
			
			DATA.type = JT_BOOL;
			DATA.bool = 1;

			break;
		// false
		case 'f':
			STEP = PS_BOOL;
			BOOL_STATE.length = 1;
			
			DATA.type = JT_BOOL;
			DATA.bool = 0;

			break;
		// Object
		case '{':
			STEP = PS_OBJECT;
			OBJECT_STATE.step = OS_START;
			OBJECT_STATE.key = NULL;

			DATA.type = JT_OBJECT;
			JSONObjectInit(&(DATA.object), JOT_DEFAULT);

			break;
		// Array
		case '[':
			STEP = PS_ARRAY;
			ARRAY_STATE.step = AS_START;

			DATA.type = JT_ARRAY;
			JSONArrayInit(&(DATA.array), JAT_DEFAULT);

			break;
		// null
		case 'n':
			STEP = PS_NULL;
			NULL_STATE.length = 1;
			
			DATA.type = JT_NULL;

			break;
		// undefined
		case 'u':
			STEP = PS_UNDEFINED;
			UNDEFINED_STATE.length = 1;
			
			DATA.type = JT_UNDEFINED;

			break;
		// Other
		default:
			// Number
			if (('0' <= json[*i] && json[*i] <= '9') || json[*i] == '-') {
				STEP = PS_NUMBER;
				NUMBER_STATE.dot = 0;
				NUMBER_STATE.exp = 0;
				NUMBER_STATE.e = 0;
				NUMBER_STATE.decimalCount = 0;

				if (json[*i] == '-') {
					NUMBER_STATE.negative = 1;
					NUMBER_STATE.tmp = 0;
					NUMBER_STATE.ready = 0;
				} else {
					NUMBER_STATE.negative = 0;
					DATA.number = NUMBER_STATE.tmp = json[*i] - '0';
					NUMBER_STATE.ready = 1;
				}

				DATA.type = JT_NUMBER;

				break;
			}

			return JE_BADCHAR;
	}

	return JE_NOERR;
}
JSONError parseStringStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

#define appendChar(c) \
	if (STRING_STATE.length >= STRING_STATE.allocSize) {\
		STRING_STATE.allocSize = STRING_ALLOC_STEP(STRING_STATE.allocSize);\
		DATA.string = (char *)realloc(DATA.string, STRING_STATE.allocSize);\
	}\
	DATA.string[STRING_STATE.length++] = c;

	for (; *i < length; (*i)++) {
		// Invalid character
		if (iscntrl(json[*i])) {
			return JE_BADCHAR;
		}
		// Parse escape sequence
		if (STRING_STATE.esc) {
			// Parse unicode escape sequence
			if (STRING_STATE.uni) {
				char u = hexToNum(json[*i]);

				if (u == -1) return JE_BADSTRING;

				STRING_STATE.unicode <<= 4;
				STRING_STATE.unicode |= u;

				if (STRING_STATE.uniLen < 3) {
					STRING_STATE.uniLen += 1;

					continue;
				}
				
				if (STRING_STATE.unicode < 0x80) { // One byte
					appendChar((char)STRING_STATE.unicode);
				} else if (STRING_STATE.unicode < 0x800) { // Two bytes
					appendChar(0b11000000 | ((STRING_STATE.unicode >> 6) & 0b11111));
					appendChar(0b10000000 | (STRING_STATE.unicode & 0b111111));
				} else { // Three bytes
					appendChar(0b11100000 | ((STRING_STATE.unicode >> 12) & 0b1111));
					appendChar(0b10000000 | ((STRING_STATE.unicode >> 6) & 0b111111));
					appendChar(0b10000000 | (STRING_STATE.unicode & 0b111111));
				}

				STRING_STATE.esc = 0;

				continue;
			}
#define caseEsc(esc, c) case esc: appendChar(c); break;
			switch (json[*i]) {
				caseEsc('"', '"')
				caseEsc('\\', '\\')
				caseEsc('/', '/')
				caseEsc('b', '\b')
				caseEsc('f', '\f')
				caseEsc('n', '\n')
				caseEsc('r', '\r')
				caseEsc('t', '\t')
				// Unicode escape sequence
				case 'u':
					STRING_STATE.uni = 1;
					STRING_STATE.uniLen = 0;
					continue;
				default:
					return JE_BADSTRING;
			}

			STRING_STATE.esc = 0;

			continue;
#undef caseEsc
		}
		// Start of esapce sequence
		if (json[*i] == '\\') {
			STRING_STATE.esc = 1;
			STRING_STATE.uni = 0;
			continue;
		}
		// End of string
		if (json[*i] == '"') {
			appendChar(0);	// Null byte
			STEP = PS_END;
			return JE_NOERR;
		}

		appendChar(json[*i]);
	}
#undef appendChar

	return JE_NOERR;
}
JSONError parseNumberStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	for (; *i < length; (*i)++) {
		// The previous character was 'e'
		if (NUMBER_STATE.afterExp) {
			NUMBER_STATE.afterExp = 0;
			// Power of ten is positive
			if (json[*i] == '+') {
				NUMBER_STATE.negExp = 0;
				continue;
			}
			// Power of ten is negative
			if (json[*i] == '-') {
				NUMBER_STATE.negExp = 1;
				continue;
			}
		}
		// Digit
		if ('0' <= json[*i] && json[*i] <= '9') {
			// The number is ready to be retrieved
			NUMBER_STATE.ready = 1;
			// Parse power of ten
			if (NUMBER_STATE.exp) {
				NUMBER_STATE.e *= 10;
				NUMBER_STATE.e += json[*i] - '0';
				continue;
			}

			NUMBER_STATE.tmp *= 10;
			NUMBER_STATE.tmp += json[*i] - '0';
			if (NUMBER_STATE.dot) {
				NUMBER_STATE.decimalCount += 1;
			}
			continue;
		}
		// Dot
		if (json[*i] == '.' && !NUMBER_STATE.dot && !NUMBER_STATE.exp) {
			NUMBER_STATE.dot = 1;
			NUMBER_STATE.ready = 0;
			continue;
		}
		// Power of ten
		if (json[*i] == 'e' && !NUMBER_STATE.exp) {
			NUMBER_STATE.exp = 1;
			NUMBER_STATE.afterExp = 1;
			NUMBER_STATE.ready = 0;
			continue;
		}
		// End of number
		(*i)--;
		STEP = PS_END;
		break;
	}
	// Calculates the value of the number
	DATA.number = (double)(NUMBER_STATE.tmp) * pow(10, (long)(NUMBER_STATE.e * (NUMBER_STATE.negExp ? -1 : 1) - NUMBER_STATE.decimalCount)) * (NUMBER_STATE.negative ? -1 : 1);

	return JE_NOERR;
}
JSONError parseBoolStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	for (; *i < length; (*i)++) {
		// Parse "true"
		if (DATA.bool) {
			if (json[*i] != TRUE_STR[BOOL_STATE.length]) return JE_BADCHAR;

			if (++BOOL_STATE.length == strlen(TRUE_STR)) {
				STEP = PS_END;
				break;
			}
			continue;
		}
		// Parse "false"
		if (json[*i] != FALSE_STR[BOOL_STATE.length]) return JE_BADCHAR;

		if (++BOOL_STATE.length == strlen(FALSE_STR)) {
			STEP = PS_END;
			break;
		}
	}

	return JE_NOERR;
}
JSONError parseObjectStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	while (*i < length && isspace(json[*i])) (*i)++;
	if (*i >= length) return JE_NOERR;

	switch (json[*i]) {
		// Key
		case '"': {
			if (OBJECT_STATE.step != OS_START && OBJECT_STATE.step != OS_AFTER) return JE_BADCHAR;

			OBJECT_STATE.step = OS_COLON;

			// Initialize child state to parse a string
			struct __ParserState *state = (struct __ParserState *)malloc(sizeof(struct __ParserState));

			state->parent = STATE;
			STATE = state;

			STEP = PS_STRING;
			STRING_STATE.esc = 0;
			STRING_STATE.length = 0;
			STRING_STATE.allocSize = STRING_ALLOC_INIT;
			
			DATA.type = JT_STRING;
			DATA.string = (char *)malloc(STRING_ALLOC_INIT);

			break;
		}
		// Colon
		case ':': {
			if (OBJECT_STATE.step != OS_COLON) return JE_BADCHAR;

			OBJECT_STATE.step = OS_END;

			// Initialize child state to parse a value
			struct __ParserState *state = (struct __ParserState *)malloc(sizeof(struct __ParserState));

			state->parent = STATE;
			STATE = state;

			STEP = PS_START;
			
			DATA.type = JT_UNDEFINED;

			break;
		}
		// Comma
		case ',':
			if (OBJECT_STATE.step != OS_END) return JE_BADCHAR;

			OBJECT_STATE.step = OS_AFTER;
			break;
		// End of object
		case '}':
			if (OBJECT_STATE.step != OS_START && OBJECT_STATE.step != OS_END) return JE_BADCHAR;

			STEP = PS_END;
			break;
		// Invalid character
		default:
			return JE_BADCHAR;
	}

	return JE_NOERR;
}
JSONError parseArrayStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	while (*i < length && isspace(json[*i])) (*i)++;
	if (*i >= length) return JE_NOERR;

	switch (json[*i]) {
		// Other character
		default:
			if (ARRAY_STATE.step != AS_START) return JE_BADCHAR;

			ARRAY_STATE.step = AS_END;

			(*i)--;
		// Colon
		case ',': {
			if (ARRAY_STATE.step != AS_END) return JE_BADCHAR;

			ARRAY_STATE.step = AS_END;

			// Initialize child state to parse element
			struct __ParserState *state = (struct __ParserState *)malloc(sizeof(struct __ParserState));

			state->parent = STATE;
			STATE = state;

			STEP = PS_START;
			
			DATA.type = JT_UNDEFINED;

			break;
		}
		// End of array
		case ']':
			STEP = PS_END;
			break;
	}

	return JE_NOERR;
}
JSONError parseNullStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	for (; *i < length; (*i)++) {
		if (json[*i] != NULL_STR[NULL_STATE.length]) return JE_BADCHAR;

		if (++NULL_STATE.length == strlen(NULL_STR)) {
			STEP = PS_END;
			break;
		}
	}

	return JE_NOERR;
}
JSONError parseUndefinedStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	for (; *i < length; (*i)++) {
		if (json[*i] != UNDEFINED_STR[UNDEFINED_STATE.length]) return JE_BADCHAR;

		if (++UNDEFINED_STATE.length == strlen(UNDEFINED_STR)) {
			STEP = PS_END;
			break;
		}
	}

	return JE_NOERR;
}
JSONError parseEndStream(const char *json, unsigned long length, unsigned long *i, struct __ParserState **parserState) {
	if (json == NULL || i == NULL || parserState == NULL || STATE == NULL) return JE_NULLPARAM;

	// If this is the root, return an error if a non white space character is detected
	if (STATE->parent == NULL) {
		while (*i < length && isspace(json[*i])) (*i)++;
		if (*i >= length) return JE_NOERR;

		return JE_BADCHAR;
	}

	// Because there is a (*i)++ in the JSONParse function
	(*i)--;
	// Check if the parent is an object or an array
	switch (STATE->parent->step) {
		// Object
		case PS_OBJECT:
			// Check if a key or a value was parsed
			switch (STATE->parent->object.step) {
				// Parsed the key, free the state, OBJECT_STATE.key is already set to DATA.string
				case OS_COLON: {
					struct __ParserState *state = STATE->parent;

					state->object.key = DATA.string;

					free(STATE);
					STATE = state;

					break;
				}
				// Parsed the value, set the item and free the state
				case OS_END: {
					struct __ParserState *state = STATE->parent;

					JSONObjectSet(state->data.object, state->object.key, DATA);

					free(state->object.key);
					state->object.key = NULL;
					free(STATE);
					STATE = state;

					break;
				}
				default: return JE_BADPARAM;
			}
			break;
		// Array
		case PS_ARRAY:
			// Push the item and free the state
			struct __ParserState *state = STATE->parent;

			JSONArrayPush(state->data.array, DATA);

			free(STATE);
			STATE = state;
			break;
		default: return JE_BADPARAM;
	}

	return JE_NOERR;
}


JSONError __freeState(struct __ParserState *state) {
	if (state == NULL) return JE_NULLPARAM;

	if (state->data.type == JT_OBJECT && state->object.key != NULL) free(state->object.key);
	JSONDataClean(state->data);

	free(state);

	return JE_NOERR;
}


static inline char hexToNum(const char c) {
	return '0' <= c && c <= '9' ? c - '0' :
		'A' <= c && c <= 'F' ? c - 'A' + 10 :
		'a' <= c && c <= 'f' ? c - 'a' + 10 :
		-1;
}


// Undefine all the constants not to pollute the macro definitions
#undef STATE
#undef STRING_STATE
#undef NUMBER_STATE
#undef BOOL_STATE
#undef OBJECT_STATE
#undef ARRAY_STATE
#undef NULL_STATE
#undef UNDEFINED_STATE

#undef TRUE_STR
#undef FALSE_STR
#undef NULL_STR
#undef UNDEFINED_STR

#undef STRING_ALLOC_STEP
#undef STRING_ALLOC_INIT
