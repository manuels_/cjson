#include <stdlib.h>
#include <string.h>

#include "object.h"


// Linked list key/value pair
typedef struct __ObjLinked {
	char *key;
	JSONData value;
	struct __ObjLinked *next;
} __ObjLinked;

// Hash table default size
#define JSON_OBJECT_HASH_TABLE_DEFAULT_SIZE 32

// JSONObject definition
struct __JSONObject {
	// Type of JSONObject used internally
	JSONObjectType type;
	union {
		struct {
			// Allocated hash table size
			unsigned long tableSize;
			__ObjLinked **hashTable;
		};
		__ObjLinked *linkedList;
	};
};



// Hash function that takes a string as input and outputs a number used by the hash table
unsigned long hashFunction(const unsigned char *str) {
	if (str == NULL) return 0;

	unsigned long hash = 5381;
	int c;

	while (c = *str++)
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

// Functions to free a linked list and an item (definition at the end of the file)
JSONError __freeLinkedList(__ObjLinked *item);
#define __freeItem(item) { free(item->key); free(item); }


#define find(while_condition, next, find_condition, ...) \
	while (while_condition) {\
		if (find_condition) {\
			__VA_ARGS__\
		}\
		next;\
	}
#define findKey(item, search_key, ...) find(item != NULL, item = item->next, (!strcmp(search_key, item->key)), __VA_ARGS__)
#define findKeyP(item, search_key, ...) find(*item != NULL, item = &((*item)->next), (!strcmp(search_key, (*item)->key)), __VA_ARGS__)


// Initialize the JSONObject by allocating the amount of memory required for each type
JSONError JSONObjectInit(JSONObject *obj, JSONObjectType type) {
	if (obj == NULL) return JE_NULLPARAM;

	switch (type) {
		// Hash table type
		case JOT_HASH:
			return JSONObjectInitHash(obj, JSON_OBJECT_HASH_TABLE_DEFAULT_SIZE);
		// Linked list type
		case JOT_LINKED:
			return JSONObjectInitLinked(obj);
		// Invalid type
		default:
			return JE_BADPARAM;
	}
}
// Inititalize a JSONObject with the hash table type of a given size
JSONError JSONObjectInitHash(JSONObject *obj, unsigned long size) {
	if (obj == NULL) return JE_NULLPARAM;

	*obj = (JSONObject)malloc(sizeof(struct __JSONObject));
	if (*obj == NULL) return JE_ALLOC;
	(*obj)->type = JOT_HASH;

	(*obj)->hashTable = (__ObjLinked **)malloc(sizeof(__ObjLinked *) * size);
	if ((*obj)->hashTable == NULL) {
		free(*obj);
		return JE_NULLPARAM;
	}
	(*obj)->tableSize = size;
	// Set all the Items to NULL
	memset((*obj)->hashTable, 0, sizeof(__ObjLinked *) * size);

	return JE_NOERR;
}
// Initialize a JSONObject with the linked list type
JSONError JSONObjectInitLinked(JSONObject *obj) {
	if (obj == NULL) return JE_NULLPARAM;

	*obj = (JSONObject)malloc(sizeof(struct __JSONObject));
	if (*obj == NULL) return JE_ALLOC;
	(*obj)->type = JOT_LINKED;

	// Initialize it with no item
	(*obj)->linkedList = NULL;

	return JE_NOERR;
}
// Clean the JSONObject
JSONError JSONObjectClean(JSONObject obj) {
	if (obj == NULL) return JE_NULLPARAM;

	int freeData(const char *key, JSONData data, JSONObject obj) {
		JSONDataClean(data);
		return 0;
	}

	JSONObjectForEach(obj, freeData);
	return JSONObjectFree(obj);
}
// Free the JSONObject
JSONError JSONObjectFree(JSONObject obj) {
	if (obj == NULL) return JE_NULLPARAM;

	__ObjLinked *item;
	JSONError err = JE_NOERR;

	switch (obj->type) {
		// Hash table type
		case JOT_HASH:
			// Loop through each linked list
			for (unsigned long i = 0; i < obj->tableSize; i++)
				err |= __freeLinkedList(obj->hashTable[i]);
			break;
		// Linked list type
		case JOT_LINKED:
			err = __freeLinkedList(obj->linkedList);
			break;
		// Invalid type
		default:
			return JE_BADPARAM;
	}

	free(obj);

	return err;
}
// Set or adds a value with the specified key in the JSONObject
JSONError JSONObjectSet(JSONObject obj, const char *key, JSONData value) {
	if (obj == NULL || key == NULL) return JE_NULLPARAM;

	__ObjLinked **itemP; // Pointer to the pointer of the next value so that we can change the value of the pointer

	switch (obj->type) {
		// Hash table type
		case JOT_HASH:
			itemP = &(obj->hashTable[hashFunction(key) % obj->tableSize]); // Get the linked list at the right index
			break;
		// Linked list type
		case JOT_LINKED:
			itemP = &(obj->linkedList); // First item of the linked list
			break;
		// Invalid type
		default:
			return JE_BADPARAM;
	}
	// Search for an item that has the the key in the linked list
	findKeyP(itemP, key,
		(*itemP)->value = value;
		return JE_NOERR;
	)
	// Didn't find a matching key : adding a new itemP

	*itemP = (__ObjLinked *)malloc(sizeof(__ObjLinked));
	if (*itemP == NULL) return JE_ALLOC;

	(*itemP)->key = (char *)malloc(strlen(key)+1);
	if ((*itemP)->key == NULL) return JE_ALLOC;
	strcpy((*itemP)->key, key);

	(*itemP)->value = value;
	(*itemP)->next = NULL;

	return JE_NOERR;
}
// Get a pointer to the value with the specified key in the JSONObject. If it doesn't find one, returns undefined
JSONError JSONObjectGetP(JSONObject obj, const char *key, JSONData **valuep) {
	if (obj == NULL || key == NULL || valuep == NULL) return JE_NULLPARAM;

	__ObjLinked *item;

	switch (obj->type) {
		// Hash table type
		case JOT_HASH:
			item = obj->hashTable[hashFunction(key) % obj->tableSize];
			break;
		// Linked list type
		case JOT_LINKED:
			item = obj->linkedList;
			break;
		// Invalid type
		default:
			return JE_BADPARAM;
	}
	// Search for the item with the key
	findKey(item, key,
		*valuep = &(item->value);
		return JE_NOERR;
	)
	// Else return NULL
	*valuep = NULL;
	return JE_NOERR;
}
// Get a value with the specified key in the JSONObject. If it doesn't find one, returns undefined
JSONError JSONObjectGet(JSONObject obj, const char *key, JSONData *value) {
	if (obj == NULL || key == NULL || value == NULL) return JE_NULLPARAM;

	JSONData *data;

	JSONError err = JSONObjectGetP(obj, key, &data);

	if (err != JE_NOERR) return err;

	if (data == NULL) *value = (JSONData){ JT_UNDEFINED, NULL };
	else *value = *data;

	return JE_NOERR;
}
// Remove the item with the specified key from the JSONObject and returns the value if value is not NULL
JSONError JSONObjectRemove(JSONObject obj, const char *key, JSONData *value) {
	if (obj == NULL || key == NULL) return JE_NULLPARAM;

	__ObjLinked **itemP; // Pointer to the pointer of the next value so that we can change the pointer to the next value
	
	switch (obj->type) {
		// Hash table type
		case JOT_HASH:
			itemP = &(obj->hashTable[hashFunction(key) % obj->tableSize]);
			break;
		// Linked list type
		case JOT_LINKED:
			itemP = &(obj->linkedList);
			break;
		// Invalid type
		default:
			return JE_BADPARAM;
	}
	// Search for the item with the matching key
	findKeyP(itemP, key,
		__ObjLinked *item;

		item = *itemP;
		*itemP = item->next;

		if (value != NULL)
			*value = item->value;

		__freeItem(item);

		return JE_NOERR;
	)

	if (value != NULL)
		*value = (JSONData){ JT_UNDEFINED, NULL };

	return JE_NOERR;
}
// Internal ForEach function because JSONObjectForEach and JSONObjectMap are almost identical
JSONError __ObjForEach(JSONObject obj, int(*fn)(), int pointerArg) {
	if (obj == NULL || fn == NULL) return JE_NULLPARAM;

	__ObjLinked *item;

	switch (obj->type) {
		// Hash table type
		case JOT_HASH:
			for (unsigned long i = 0; i < obj->tableSize; i++) {
				for (item = obj->hashTable[i]; item != NULL; item = item->next) {
					if (pointerArg) {
						if (fn(item->key, &(item->value), obj)) goto break_fors;
					} else if (fn(item->key, item->value, obj)) goto break_fors;
				}
			}
break_fors:
			break;
		// Linked list type
		case JOT_LINKED:
			for (item = obj->linkedList; item != NULL; item = item->next) {
				if (pointerArg) {
					if (fn(item->key, &(item->value), obj)) break;
				} else if (fn(item->key, item->value, obj)) break;
			}
			break;
		default:
			return JE_BADPARAM;
	}

	return JE_NOERR;
}
// Calls the function for each item in the object
JSONError JSONObjectForEach(JSONObject obj, int(*fn)(const char *, JSONData, JSONObject)) {
	return __ObjForEach(obj, (int(*)())fn, 0);
}
// Calls the function for each item in the object passing a pointer to the JSONData so it can be modified
JSONError JSONObjectMap(JSONObject obj, int(*fn)(const char *, JSONData *, JSONObject)) {
	return __ObjForEach(obj, (int(*)())fn, 1);
}

// Free a linked list
JSONError __freeLinkedList(__ObjLinked *item) {
	__ObjLinked *_item;

	// Loop through all the items
	while (item != NULL) {
		_item = item; // Save it in another variable because if I free the item before getting the next item it will cause indefined behaviour

		item = item->next;

		__freeItem(_item);
	}

	return JE_NOERR;
}
