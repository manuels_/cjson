#ifndef OBJECT_H
#define OBJECT_H

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


typedef enum JSONObjectType {
	JOT_HASH,
	JOT_LINKED,
} JSONObjectType;
#define JOT_DEFAULT JOT_HASH


typedef struct __JSONObject *JSONObject;


JSONError JSONObjectInit(JSONObject *obj, JSONObjectType type);
JSONError JSONObjectInitHash(JSONObject *obj, unsigned long size);
JSONError JSONObjectInitLinked(JSONObject *obj);

JSONError JSONObjectClean(JSONObject obj);
JSONError JSONObjectFree(JSONObject obj);

JSONError JSONObjectGetP(JSONObject obj, const char *key, JSONData **valuep);
JSONError JSONObjectGet(JSONObject obj, const char *key, JSONData *value);
JSONError JSONObjectSet(JSONObject obj, const char *key, JSONData value);
JSONError JSONObjectRemove(JSONObject obj, const char *key, JSONData *value);

JSONError JSONObjectForEach(JSONObject obj, int(*fn)(const char *, JSONData, JSONObject));
JSONError JSONObjectMap(JSONObject obj, int(*fn)(const char *, JSONData *, JSONObject));


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !OBJECT_H
