#ifndef PARSER_H
#define PARSER_H

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


JSONError JSONParse(const char *json, unsigned long length, JSONData *data);


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !PARSER_H
