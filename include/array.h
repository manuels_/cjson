#ifndef ARRAY_H
#define ARRAY_H

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


typedef enum JSONArrayType {
	JAT_TABLE,
	JAT_LINKED,
} JSONArrayType;
#define JAT_DEFAULT JAT_LINKED


typedef struct __JSONArray *JSONArray;


JSONError JSONArrayInit(JSONArray *arr, JSONArrayType type);
JSONError JSONArrayInitTable(JSONArray *arr, unsigned long size);
JSONError JSONArrayInitLinked(JSONArray *arr);

JSONError JSONArrayClean(JSONArray arr);
JSONError JSONArrayFree(JSONArray arr);

unsigned long JSONArrayLength(JSONArray arr);

JSONError JSONArrayGetP(JSONArray arr, unsigned long index, JSONData **valuep);
JSONError JSONArrayGet(JSONArray arr, unsigned long index, JSONData *value);
JSONError JSONArraySet(JSONArray arr, unsigned long index, JSONData value);
JSONError JSONArrayRemove(JSONArray arr, unsigned long index, JSONData *value);

JSONError JSONArrayPush(JSONArray arr, JSONData value);
JSONError JSONArrayPop(JSONArray arr, JSONData *value);

JSONError JSONArrayForEach(JSONArray arr, int(*fn)(JSONData, unsigned long, JSONArray));
JSONError JSONArrayMap(JSONArray arr, int(*fn)(JSONData *, unsigned long, JSONArray));


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !OBJECT_H
