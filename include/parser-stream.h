#ifndef PARSER_H
#define PARSER_H

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


typedef struct __ParserState *JSONParserState;


#define JSON_PARSER_STATE_INIT 0


JSONError JSONParseStream(const char *json, unsigned long length, JSONParserState *parserState);

JSONError JSONParserClean(JSONParserState state);
JSONError JSONParserGetData(JSONParserState state, JSONData *data);


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !PARSER_H
