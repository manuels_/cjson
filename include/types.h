#ifndef TYPES_H
#define TYPES_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


typedef enum JSONError {
	JE_NOERR,
	JE_ALLOC,
	JE_NULLPARAM,
	JE_OUTOFRANGE,
	JE_BADPARAM,
	JE_BADCHAR,
	JE_BADSTRING,
	JE_BADNUMBER,
	JE_BADOBJECT,
	JE_BADARRAY
} JSONError;


typedef enum {
	JT_NUMBER,
	JT_BOOL,
	JT_STRING,
	JT_OBJECT,
	JT_ARRAY,
	JT_NULL,
	JT_UNDEFINED
} JSONType;


const char *JSONErrorToString(JSONError error);

const char *JSONTypeToString(JSONType type);


typedef struct JSONData {
	JSONType type;
	union {
		char *string;
		double number;
		unsigned char bool;
		struct __JSONObject *object;
		struct __JSONArray *array;
	};
} JSONData;


JSONError JSONDataClean(JSONData data);


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // !TYPES_H
